import csv
import json
import sys

# Read CSV file and convert to JSON
def filter_data(csv_file_path, json_file_path, threshold):
    filtered_data = []
    with open(csv_file_path) as file:
        reader = csv.DictReader(file)
        for row in reader:
            if int(row['Points']) > threshold:
                filtered_data.append(row)

    with open(json_file_path, 'w') as jsonf:
        json.dump(filtered_data, jsonf, indent=4)

if __name__ == "__main__":
    threshold = int(sys.argv[1])  # Argument for points threshold
    filter_data('data.csv', 'filtered_data.json', threshold)